"""Platzigram URLs module."""

# Django
from django.contrib import admin
from django.urls import path
from django.http import HttpResponse
from platzigram import views as local_views

# apps
from posts import views as posts_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('hello-world', local_views.hello_world),
		path('sorted/', local_views.sorted_int),
		path('hi/<str:name>/<int:age>', local_views.say_hi),

    # app
    path('post/', posts_views.list_post)
]
