""" Platzigram views. """

from django.http import HttpResponse

# Utilities
from datetime import datetime
import json

def hello_world(request):
  """Return a greeting"""
  now = datetime.now().strftime('%b %dth, %Y - %H:%M hrs')
  return HttpResponse('Hello, World! Current Server time is {now}'.format(now=str(now)))


def sorted_int(request):
  """Return a JSON response with sorted integer."""
  # print(request.GET[])
  # import pdb; pdb.set_trace()
  # return HttpResponse("Hi! Man")
  # numbers = request.GET['numbers']
  numbers = [int(i) for i in request.GET['numbers'].split(",")]
  sorted_insts = sorted(numbers)
  data = {
    'status': 'ok',
    'numbers': sorted_insts,
    'message': 'Integers sorted succesfully.'
  }
  # import pdb; pdb.set_trace()
  return HttpResponse(json.dumps(data, indent = 4), content_type='application/json')


def say_hi(request, name, age):
  """return a greeting."""
  if age < 12:
    message = 'Sorry {}, you are not allowd here'.format(name)
  else:
    message = 'Hello, {}! Welcome to Platzigram'.format(name)
  
  return HttpResponse(message)
  