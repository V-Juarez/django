"""Post views"""

# Django
from django.http import HttpResponse

# Utilities
from datetime import datetime

posts = [
  {
    'name': 'Mont Blanc',
    'user': 'Yesica Cortes',
    'timestamp': datetime.now().strftime('%b %dth, %Y - %H:%M hrs'),
    'picture': 'https://picsum.photos/200/200/?image=1035',
  },
  {
    'name': 'Via Lactea',
    'user': 'C. Vander',
    'timestamp': datetime.now().strftime('%b %dth, %Y - %H:%M hrs'),
    'picture': 'https://picsum.photos/200/200/?image=903',
  },
  {
    'name': 'Nuevo Auditorio',
    'user': 'Thespianartist',
    'timestamp': datetime.now().strftime('%b %dth, %Y - %H:%M hrs'),
    'picture': 'https://picsum.photos/200/200/?image=1076',
  }
]


def list_post(request):
  """List existing posts."""

  content= []
  
  for post in posts:
    content.append("""
      <p><strong>{name}</strong></p>
      <p><small>{user} - <i>{timestamp}</i></small></p>
      <figure><img src="{picture}"/></figure>
    """)
  return HttpResponse('<br>'.join(content))