# Normalización

**DESCRIPCION DE NORMALIZACION**
La normalización es el proceso de organizar datos en una base de datos. Esto incluye la creación de tablas y el establecimiento de relaciones entre esas tablas de acuerdo con las reglas diseñadas tanto para proteger los datos como para que la base de datos sea más flexible mediante la eliminación de la redundancia y la dependencia incoherente.

Los datos redundantes desperdician espacio en disco y crean problemas de mantenimiento. Si es necesario cambiar los datos que se encuentran en más de un lugar, los datos deben cambiarse exactamente de la misma forma en todas las ubicaciones. El cambio de dirección de un cliente es mucho más fácil de implementar si los datos se almacenan solo en la tabla clientes y en ninguna otra parte de la base de datos.

¿Qué es una “dependencia incoherente”? Aunque resulta intuitivo para un usuario buscar en la tabla de clientes la dirección de un cliente en particular, es posible que no tenga sentido buscar en él el salario del empleado que llama a ese cliente. El salario del empleado está relacionado con el empleado o depende de él y, por lo tanto, se debe mover a la tabla empleados. Las dependencias incoherentes pueden dificultar el acceso a los datos, ya que la ruta para encontrar los datos puede faltar o estar dañada.

Hay algunas reglas para la normalización de bassudo pacman -S sqlitebrowseres de datos. Cada regla recibe el nombre de “formulario normal”. Si se observa la primera regla, se dice que la base de datos está en la “primera forma normal”. Si se cumplen las tres primeras reglas, se considera que la base de datos está en la “tercera forma normal”. Aunque son posibles otros niveles de normalización, la tercera forma normal se considera el nivel más alto necesario para la mayoría de las aplicaciones.

Al igual que con muchas reglas y especificaciones formales, los escenarios del mundo real no siempre permiten el cumplimiento perfecto. En general, la normalización requiere tablas adicionales y algunos clientes lo consideran engorrosos. Si decide infringir una de las tres primeras reglas de normalización, asegúrese de que la aplicación anticipa los problemas que puedan producirse, como datos redundantes y dependencias incoherentes.

En las siguientes descripciones se incluyen ejemplos.

**PRIMERA FORMA NORMAL**

- Eliminar grupos de repetición en tablas individuales.
- Cree una tabla independiente para cada conjunto de datos relacionados.
- Identifique cada conjunto de datos relacionados con una clave principal.

No use varios campos en una sola tabla para almacenar datos similares. Por ejemplo, para realizar un seguimiento de un elemento de inventario que puede provenir de dos orígenes posibles, un registro de inventario puede contener campos para el código de proveedor 1 y el código de proveedor 2.

¿Qué sucede cuando se agrega un tercer proveedor? Agregar un campo no es la respuesta; requiere modificaciones en el programa y en la tabla y no admite de forma fluida un número dinámico de proveedores. En su lugar, inserte toda la información del proveedor en una tabla separada llamada proveedores y, a continuación, vincule inventario a proveedores con una clave de número de artículo o proveedores al inventario con una clave de código de proveedor.

**SEGUNDA FORMA NORMAL**

- Crear tablas independientes para conjuntos de valores que se aplican a varios registros.
- Relacione estas tablas con una clave externa.

Los registros no deben depender de nada que no sea la clave principal de una tabla (una clave compuesta, si es necesario). Por ejemplo, considere la dirección de un cliente en un sistema de contabilidad. La dirección es necesaria para la tabla clientes, pero también las tablas pedidos, envíos, facturas, cuentas a cobrar y colecciones. En lugar de almacenar la dirección del cliente como una entrada independiente en cada una de estas tablas, almacénela en un lugar, ya sea en la tabla customers (clientes) o en una tabla de direcciones independiente.

**TERCERA FORMA NORMAL**

- Eliminar los campos que no dependen de la clave.

Los valores de un registro que no forman parte de la clave de ese registro no pertenecen a la tabla. En general, siempre que el contenido de un grupo de campos se aplique a más de un registro de la tabla, considere la posibilidad de colocar dichos campos en una tabla independiente.

Por ejemplo, en una tabla de contratación de empleados, puede incluirse el nombre de la Universidad y la dirección de un candidato. Pero necesita una lista completa de las universidades para los correos de grupo. Si la información de la Universidad se almacena en la tabla candidatos, no hay forma de enumerar universidades sin candidatos actuales. Cree una tabla universidades independiente y vincúlelo a la tabla candidatos con una clave de código de Universidad.

EXCEPCIÓN: cumplir con la tercera forma normal, aunque teóricamente deseable, no siempre es práctico. Si tiene una tabla Customers y desea eliminar todas las dependencias entre campos posibles, debe crear tablas independientes para ciudades, códigos postales, representantes de ventas, clases de clientes y cualquier otro factor que se pueda duplicar en varios registros. En teoría, la normalización merece la pena pursing. Sin embargo, muchas tablas pequeñas pueden degradar el rendimiento o superar las capacidades de memoria y de archivos abiertos.

Es posible que sea más factible aplicar la tercera forma normal solo a los datos que cambian con frecuencia. Si se conservan algunos campos dependientes, diseñe la aplicación para exigir al usuario que compruebe todos los campos relacionados cuando se modifique cualquiera de ellos.

**EJEMPLO**
En estos pasos se muestra el proceso de normalización de una tabla de alumnos ficticia.

1. Tabla no normalizada:
   ![Captura.JPG](https://static.platzi.com/media/user_upload/Captura-344c7ee8-54b1-49e4-83ae-7687b75dfa3b.jpg)
2. Primera forma normal: sin grupos de repetición
   Las tablas solo deben tener dos dimensiones. Como un estudiante tiene varias clases, estas clases deben aparecer en una tabla independiente. Los campos Class1, clase2 y Class3 en los registros anteriores son indicaciones de problemas de diseño.

Las hojas de cálculo suelen usar la tercera dimensión, pero las tablas no deben. Otra forma de analizar este problema es con una relación de uno a varios, no ponga en el lado uno y en el lado varios de la misma tabla. En su lugar, cree otra tabla en la primera forma normal eliminando el grupo extensible (número de clase), como se muestra a continuación:
![Captura.JPG](https://static.platzi.com/media/user_upload/Captura-ed58037c-3122-4437-9087-a32d7baa7731.jpg)

1. Segunda forma normal: elimine datos redundantes
   Tenga en cuenta los valores de varias clases # para cada valor de número de alumno de la tabla anterior. La clase # no depende funcionalmente del número de estudiante (clave principal), por lo que esta relación no está en la segunda forma normal.

En las dos tablas siguientes se muestra la segunda forma normal:

Deben
![Captura.JPG](https://static.platzi.com/media/user_upload/Captura-4e1ba641-3000-4efa-9bc4-b0f895a394e3.jpg)

Registro
![Captura.JPG](https://static.platzi.com/media/user_upload/Captura-92b329c2-f396-4b73-918f-3d73eeadd227.jpg)

1. Tercera forma normal: elimina datos que no dependen de la clave
   En el último ejemplo, el salón de avanzada (el número de la oficina del Consejero) depende funcionalmente del atributo asesor. La solución consiste en mover el atributo de la tabla Students a la tabla profesores, como se muestra a continuación:

Deben
![Captura.JPG](https://static.platzi.com/media/user_upload/Captura-5e993433-0a52-479c-badb-338b3e5ddf07.jpg)

Profesor
![Captura.JPG](https://static.platzi.com/media/user_upload/Captura-64607c0f-ebc6-4758-b298-3e072d31f963.jpg)