# from django.shortcuts import render
from django.http import HttpResponse 
from .models import Project, Task

# importar render | utiliza la carpeta templates
from django.shortcuts import render, redirect, get_object_or_404
# 400
# from django.shortcuts import get_object_or_404
# form
from .forms import CreateNewTask, CreateNewProject


# Create your views here.
def index(request):
    title = 'Django course!!'
    # import remplate
    return render(request, 'index.html', {
        'title': title
    })

def hello(request, username):
# def hello(request, id):
    # print(type(id))
    # result = id + 200 *2
    # print(type(id))
    # print(result)
    return HttpResponse("<h2>Hello %s</h2>" % username)

def about(request):
    username = 'fazt'
    return render(request, 'about.html', {
        'username': username
    })


def projects(request):
    # projects = list(Project.objects.values())
    # return JsonResponse(projects, safe=False)
    projects = Project.objects.all()
    return render(request, 'projects/projects.html', {
        'projects': projects
    })


def tasks(request):
    # task = Task.objects.get(id=id)
    # task = get_object_or_404(Task, id=id)
    # return HttpResponse('Tasks: %s' % task.title)
    tasks = Task.objects.all()
    return render(request, 'tasks/tasks.html', {
        'tasks': tasks
    })

# buscar por nombre
# def tasks(request, title):
#     # task = Task.objects.get(title=title)
#     task = get_object_or_404(Task, title=title)
#     return HttpResponse('Tasks: %s' % task.title)

def create_task(request):
    if request.method == 'GET':
        return render(request, 'tasks/create_task.html', {
            'form': CreateNewTask
        })
    else:
        Task.objects.create(title=request.POST['title'],description=request.POST['description'],
        project_id=2
                        )
        return redirect('tasks')


def create_project(request):
    if request.method == 'GET':
        return render(request, 'projects/create_project.html', {
            'form': CreateNewProject()
        })
    else:
        Project.objects.create(name=request.POST['name'])
        return redirect('projects')


# link para cada tarea
def project_detail(request, id):
    # project = Project.objects.get(id=id)
    project = get_object_or_404(Project, id=id)
    tasks = Task.objects.filter(project_id=id)
    return render(request, 'projects/detail.html', {
        'project': project,
        'tasks': tasks
    })
