# Database 

```sh
python manage.py shell
```

## shell

```sh
from myapp.models import Project, Task
```

## Create project

```python
p = Project(name="Aplicacion web usando Django")
p.save()
```

## List project

```python
# listar
Project.objects.all()

# <QuerySet [<Project: Aplicacion movil>, <Project: Aplicacion web usando Django>]>

# buscar por id
Project.objects.get(id=1)
# <Project: Aplicacion movil>

Project.objects.get(id=2)
# <Project: Aplicacion web usando Django>
```

## Add task and project

```python
# Task
from myapp.models import Project, Task

# listar
Project.objects.get(id=1)

<Project: Aplicacion movil>

# generar task
r = Project.objects.get(id=1)

r

#<Project: Aplicacion movil>

r.task_set.all()
<QuerySet []>

# add project in the task
r.task_set.create(title="descargar IDE")
<Task: descargar IDE - Aplicacion movil>

r.task_set.create(title="desarrollar login")
<Task: desarrollar login - Aplicacion movil>

# list task
 r.task_set.all()
<QuerySet [<Task: descargar IDE - Aplicacion movil>, <Task: desarrollar login - Aplicacion movil>]>
```

## filter

```python
Project.objects.filter(name__startswith="desarrollo")
<QuerySet []>

p = Project.objects

# save in variable
p.filter(name__startswith="abc")
<QuerySet []>

p.filter(name__startswith="aplicacion")
<QuerySet [<Project: Aplicacion movil>, <Project: Aplicacion web usando Django>]>
```



- [x] [Download SQLite](https://sqlitebrowser.org/dl/)

