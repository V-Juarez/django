## Crear entorno virtual

```sh
virtual env
```

## Activar entorno virtual

```sh
source venv/bin/activate
```

## Crear gitignore 

```sh
npx gitignore python
```

## Instalar django

```sh
pip install django
# django-admin --version
```

## Crear Projecto

```sh
django-admin startproject mysite . # Crea el projecto en el direcotrio actual
```

## Server

```sh
python manage.py runserver
# python manage.py runserver <Port>
python manage.py runserver 3000
```

## arbol del proyecto

```sh
 ❯ tree mysite 
mysite
├── asgi.py
├── css
├── __init__.py
├── __pycache__
│   ├── __init__.cpython-310.pyc
│   ├── settings.cpython-310.pyc
│   ├── urls.cpython-310.pyc
│   └── wsgi.cpython-310.pyc
├── settings.py
├── urls.py
└── wsgi.py

3 directories, 9 files
```

## Crear aplicacion

```sh
python manage.py startapp myapp
```

## Database sqlitebrowser

## Migrations

```sh
# python manage.py makemigration <name-projec> opcional
python manage.py makemigrations 

# migration
python manage.py migrate
```



## Modelos

```python
class Task(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField()
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
```

## shell django

```sh
python manage.py shell
```

Hora minutos 2:46:00